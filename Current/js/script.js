// show and hide the two divs
function toggle(div_id) {
	var el = document.getElementById(div_id);
	if ( el.style.display == 'none' ) {	el.style.display = 'block';}
	else {el.style.display = 'none';}
}

// get the height of the visible window, and center popup
function set_height(popUpDivVar) {
	if (typeof window.innerWidth != 'undefined') {
		vpheight = window.innerHeight;
	} else {
		vpheight = document.documentElement.clientHeight;
	}
	
	blanket_height = vpheight;
	
	var blanket = document.getElementById('js_cover');
	blanket.style.height = blanket_height + 'px';
	
	var popUpDiv = document.getElementById(popUpDivVar);
	popUpDiv_height=blanket_height/2-160;//150 is half popup's height
	popUpDiv.style.top = popUpDiv_height + 'px';
}

// get the width of the visible window, and center popup
function set_width(popUpDivVar) {
	if (typeof window.innerWidth != 'undefined') {
		vpwidth = window.innerWidth;
	} else {
		vpwidth = document.documentElement.clientWidth;
	}
	window_width = vpwidth;
	
	var popUpDiv = document.getElementById(popUpDivVar);
	window_width=window_width/2-150;//150 is half popup's width
	popUpDiv.style.left = window_width + 'px';
}

// POP up a div!
function popup() {
	set_height('js_popUpDiv');
	set_width('js_popUpDiv');
	toggle('js_cover');
	toggle('js_popUpDiv');		
}

// Submit the form to it's self.. boring... and toggle the login div
function sendForm() {
	document.getElementById("myForm").action = document.URL;
	document.getElementById("myForm").submit();
	toggle('js_cover');
	toggle('js_popUpDiv');	
	
}
